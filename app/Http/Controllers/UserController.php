<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\EmailVerification;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller{

    /**
     * try to get the current user
     * @return UserResource
     */
    public function currentUser(){
        /*return the user-data*/
        return new UserResource(Auth::user());
    }

    public function login(Request $request){

        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        /*erhalte die email-Adresse und das password aus dem Request*/
        $credentials = $request->only('email', 'password');

        /*Anmeldeversuch mit den gegebenen Daten*/
        if (!Auth::attempt($credentials)) {
            /*wenn das nicht klappt dann Fehlermeldung zurück geben!*/
            return response()->json('Die E-Mail Adresse oder das Passwort stimmen nicht mit den gespeicherten Daten überein.', 401);
        }

        /*wenn der Login-Versuch geklappt hat holen wir uns gleich mal den User damit wir weiter arbeiten können*/
        $user = User::where('email', $request->email)->first();

        /*überprüfen ob die E-Mail Adresse des Users bereits verifiziert wurde*/
        if($user->email_verified_at == null){
            return response()->json('Die E-Mail Adresse wurde noch nicht verifizert.', 401);
        }

        /*wir verwenden in diesem Beispiel den App-key aus der env-Datei als Secret.
        * Das secret ist dabei nicht der ganze Token sondern nur ein Salt. Natürlich würde auch etwas anderes funktionieren*/
        $secret = env("APP_KEY", "secret");
        
        /*Erstellen eines Tokens - sollte ein secret übergeben gebkommen */
        $token = $user->createToken($secret)->plainTextToken;

        /*zusamenbauen der Antwort*/
        $response = [
            'user' => new UserResource($user),
            'token' => $token
        ];

        return response($response, 201);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request){
        Auth::guard('web')->logout();
        return response()->json('Successfully logged out', 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request){
        $validator = Validator::make(
            $request->all(),['name' => ['required'],'email' => ['required'],'password' => ['required', 'min:8'],'confirmPassword' => 'same:password',]
        );
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);

        } else {
            $params = $request->all();
            
            $user = User::create([
                'name' => $params['name'],
                'email' => $params['email'],
                'password' => Hash::make($params['password']),
                'verification_token' => hash_hmac('sha256', Str::random(40), $params['email']),
            ]);

            /*eigentlicher Versand der E-Mail*/
            Mail::to($user->email)->send(new EmailVerification($user));
            
            return new UserResource($user);
        }
    }


    /**
    * Try to verify the user.
    *
    * @param $token
    * @return \Illuminate\Http\Response
    */
    public function verify($token){
        $user = User::where('verification_token',$token)->firstOrFail();

        if($user->email_verified_at != null){
            return response()->json('Ihre E-Mail Adresse wurde bereits erfolgreich verifizert. Sie können sich nun anmelden.', 200);
        }

        $user->email_verified_at = now();
        $user->save();

        return response()->json('Ihre E-Mail Adresse wurde erfolgreich verifizert. Sie können sich nun anmelden.', 200);

    }

}



