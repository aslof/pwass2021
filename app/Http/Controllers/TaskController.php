<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Resources\TaskResource;
use Illuminate\Support\Facades\Auth;

class TaskController extends BaseController{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        /*laden des derzeitigen Users*/
        $user = Auth::user();
        /*erhalte alle Tasks diesen Users*/
        $tasks = $user->tasks;
        /*alle Tasks des Users als TaskResource zurückgeben*/
        return TaskResource::collection($tasks);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $validated = $request->validate($this->validationRules());

        /*erhalte die übermittelten Elemente aus dem request*/
        $params = $request->all();

        /*erhalte den derzeitigen User*/
        $user = Auth::user();

        /*anlegen eines neuen Tasks*/
        $task = Task::create([
            'title' => $params['title'],
            'description' => $params['description'],
            /*ein Weg von vielen um die Zuordnung zwischen dem User und dem Task herzustellen*/
            'user_id' => $user->id,
        ]);

        return new TaskResource($task);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        /*erhalte den aktuellen User*/
        $user = Auth::user();

        /*finden eines Tasks mit einer bestimmten ID*/
        $task = Task::findOrFail($id);

        /*überprüfe ob der aktuelle User der "Besitzer" des Tasks ist*/
        if($user->id == $task->user_id){
            /*rückgabe einer TaskResource mit den Werten des Task*/
            return new TaskResource($task);
        }

        /*abbrechen der Aktion wenn der aktuelle User nicht der "Besitzer" des Tasks ist*/
        abort(400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        /*finden des Task der angepasst werden soll*/
        $task = Task::findOrFail($id);

        /*erhalte die übermittelten Elemente aus dem request*/
        $params = $request->all();

        /*überschreiben / zuweisen der Werte in dem task mit den Werten des Requests*/
        if(isset($params['title'])){
            $task->title = $params['title'];
        }

        if(isset($params['description'])){
            $task->description = $params['description'];
        }

        /*nur wenn die Checkbox gesetzt ist wird der in dem request das Element 
        * mit dem entsprechenden Namen mitgeliefert. Wenn die Checkbox nicht gesetzt ist
        * muss entsprechend angenommen werden, dass die Aufgabe nicht eledigt ist!*/
        if(isset($params['status']) && $params['status'] == true){
            $task->status = "done";
        }
        else{
            $task->status = "outstanding";
        }

        /*Übernahme der Änderungen an dem Model Task in die Datenbank*/
        $task->save();

        /*und zurückgeben des angepassten Tasks*/
        return new TaskResource($task);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        /*finden des Task der gelöscht werden soll*/
        $task = Task::findOrFail($id);
        /*löschen des Tasks*/
        $task->delete();
        /*Wenn alles geklappt hat geben wir eine http-response mit dem Wert 200 zurück*/
        return response()->json(200);
    }

    /**
     * Spezialfunktion um einen einzelnen Status für einen Task upzudaten
     * @param  $id      Die ID des Tasks
     * @param  Request $request Der Request mit dem neuen Status
     * @return nur ein ResponseCode falls alles geklappt hat
     */
    public function updateStatus($id, Request $request){
       /*finden des Task dessen Status angepasst werden soll*/
       $task = Task::findOrFail($id);

       /*erhalte die übermittelten Elemente aus dem request*/
       $params = $request->all();

       /*nachsehen ob es einen status gibt - und wenn ja setzen des Status*/
       if(isset($params['status']) && $params['status'] == true){
        $task->status = "done";
    }
    else{
        $task->status = "outstanding";
    }

    /*abspeichern der Änderungen*/
    $task->save();

    /*Wenn alles geklappt hat geben wir eine http-response mit dem Wert 200 zurück*/
    return response()->json(200);
}

    /**
     * The rules for the validation
     * @param bool $isNew
     * @return array
     */
    private function validationRules(){
        return [
            'title' => 'required|max:499',
            'description' => 'required',
        ];
    }

}
