<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Log;

class EmailVerification extends Mailable{

    use Queueable, SerializesModels;

    public $user;
    public $from;
    public $token;
    public $verification_link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user){
        $this->user = $user;
        $this->token = $user->verification_token;
        $this->verification_link = URL::to('/').'/#/verify/'.$this->token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        return $this->view('Mails.verification')
        ->subject('SimpleTasks - dein neues Konto');
    }
}

