<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model{
	use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
     	'title', 'description', 'status', 'user_id',
     ];

     /**
      * parsen des Status um die Werte einer Checkbox zuordnen zu können
      * @return Boolean
      */
     public function getStatus(){
        return $this->status=="done"?true:false;
     }


    /**
    * user who created this task
    */
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

 }

