import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import store from './store';
import router from './router';
import VueAxios from 'vue-axios';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';


// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

/* Make BootstrapVue available throughout your project */
Vue.use(BootstrapVue);
/* Optionally install the BootstrapVue icon components plugin */
Vue.use(IconsPlugin);
/*use axios to get data*/
Vue.use(VueAxios, axios);
/*use Vuex for the state management*/
Vue.use(Vuex);

/*Main pages*/
import App from './views/app.vue'

new Vue({
	el: '#app',
	store,
	router,
	render: h => h(App),
});
