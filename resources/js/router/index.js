import Vue from 'vue';

/*get access to the store*/
import store from '@/store';

/*init router*/
import Router from 'vue-router';

Vue.use(Router);

/* Layout */
import Layout from '../layout';
import LandingpageLayout from '../LandingpageLayout';

export const constantRoutes = [
{
	path: '/',
	component: Layout,
	children: [
	{
		name: 'Home',
		path: '/',
		meta: {
			auth: true
		},
		component: () => import('../views/tasklist'),
	},
	{
		name: 'Test',
		path: '/test',
		meta: {
			auth: true
		},
		component: () => import('../views/landingpage'),
	},
	],
},
{
	path: '/landingpage',
	component: LandingpageLayout,
	children: [
	{
		name: 'Login',
		path: '/login',
		component: () => import('../views/login'),
	},
	{
		name: 'Register',
		path: '/register',
		component: () => import('../views/register'),
	},

	{
		path: '/verify/:token',
		name: 'Verification',
		component: () => import('../views/login'),
	},
	
	],
}
];


const createRouter = () => new Router({
	scrollBehavior: () => ({ y: 0 }),
	routes: constantRoutes,
});

const router = createRouter();

router.beforeEach((to, from, next) => {
	const token = store.getters['getToken'];
	if (to.meta.auth == true && token === null) {
		next('/login');
		return;
	}
	next();
});

export default router;