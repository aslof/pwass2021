
import request from '@/utils/request';

export function register(data) {
  return request({
    url: '/register',
    method: 'post',
    data: data,
  });
}

export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data: data,
  });
}

export function logout() {
  return request({
    url: '/logout',
    method: 'post',
  });
}

export function csrf() {
  return request({
    url: '/sanctum/csrf-cookie',
    method: 'get',
  });
}

export function getUser() {
  return request({
    url: '/user',
    method: 'get',
  });
}

export function verify(token) {
  return request({
    url: '/user/verify/'+token,
    method: 'get',
  });
}

