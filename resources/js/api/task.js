import request from '@/utils/request';
import Resource from '@/api/resource';

/*ein Beispiel für eine spezielle Funktion die nur den Status updated*/
export function updateTaskStatus(id,status) {
	return request({
		url: '/task/'+id+'/update-status',
		method: 'put',
		data: status,
	});
}