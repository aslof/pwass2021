import '@/bootstrap';
import store from '@/store';

// Create axios instance
const service = window.axios.create({
  baseURL: process.env.MIX_BASE_API,
  timeout: 10000, // Request timeout
});

// Request intercepter
service.interceptors.request.use(
  config => {
    /*set the token for the auth-headers*/
    const token = store.getters['getToken'];
    if (token) {
      config.headers['Authorization'] = 'Bearer ' + token;
    }
    return config;
  },
  error => {
    // Do something with request error
    console.log(error); // for debug
    Promise.reject(error);
  }
  );

// response pre-processing
service.interceptors.response.use(
  response => {
    return response.data;
  },
  error => {
    console.log(error);
    let message = error.message;
    if (error.response.data && error.response.data.errors) {
      message = error.response.data.errors;
    } else if (error.response.data && error.response.data.error) {
      message = error.response.data.error;
    }

    /*if we get an errorcode 401 (unauthorized) we'll assume that the login is no longer valid so'll delete the auth-tokens*/
    if(error.response.status === 401){
      store.commit('SET_TOKEN',null);
    }

    return Promise.reject(error);
  }
  );

export default service;