import Vue from 'vue';
import Vuex from 'vuex';
import { login, getUser } from '@/api/auth';
import Resource from '@/api/resource';

const userResource = new Resource('settings');

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		user: null,
		token: null,
	},
	mutations: {
		SET_TOKEN: (state, token ) => {
			state.token = token;
			localStorage.setItem('token', token);
		},
		SET_USER: (state, user ) => {
			state.user = user;
		},
	},
	getters: {
		getUser(state) {
			return state.user;
		},
		getToken(state) {
			if(state.token === null){
				/*trying to get the token from localStorage*/
				var token = localStorage.getItem('token');
				/*set the token to the token from localStorage - but only if that token is not null*/
				if(token != null && token != "null"){
					state.token=token;
				}
			}
			return state.token;
		},
	},
	actions: {
		loadUser({ commit, getters }) {
			/*let's see if we already have the user-data - if not we'll load the user from the server*/
			if(getters.getUser == null){
				getUser().then(response => {
					commit('SET_USER', response.data);
				})
				.catch(error => {
					console.log(error);
				});
			}
		},
		login ({commit, getters},payload) {
			return new Promise((resolve, reject) => {
				/*a new login*/
				login(payload).then(response => {
					/*set the token*/
					commit('SET_TOKEN', response.token);
					/*set the user*/
					commit('SET_USER', response.user);
				})
				.catch(error => {
					reject(error.response.data)
				});
			});
		}
	},
});
