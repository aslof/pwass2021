<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="{{ mix('css/app.css') }}" />
	<link rel="stylesheet" href="{{ url('/css/custom.css') }}" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{env('APP_NAME')}}</title>
</head>
<body class="d-flex flex-column min-vh-100">
	<div id="app">
		<app></app>
	</div>

	<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
