<!DOCTYPE html>
<html lang="de-DE">
<head>
	<meta charset="utf-8">
</head>
<body>
	<div>
		<p>
			Hallo {{ $user->name }},
		</p>
		<p>
			herzlichen Glückwunsch zur besten Entscheidung deines Lebens. Mit Simple-Tasks wirst du ruck zuck deine anstehenden Projekte und Aufgaben ordnen und erledigen. Also auf zu neuen Herausforderungen und Projekten. Damit du los legen kannst musst du nur einmal auf den folgenden Link klicken um zu bestätigen, dass du diese E-Mail erhalten hast:
			<br><br>
			<a href="{{ $verification_link }}">Hier klicken und gleich los legen!</a>
		</p>
	</div>
</body>
</html>