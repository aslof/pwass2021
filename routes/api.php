<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route für den Login*/
Route::post('/login', 'App\Http\Controllers\UserController@login');
Route::post('/register', 'App\Http\Controllers\UserController@register');
Route::get('/user/verify/{token}','App\Http\Controllers\UserController@verify');

/*Alles was in dieser Route-Group steht, ist nur nach dem Login eines Users abrufbar!*/
Route::group(['middleware' => 'auth:sanctum'], function () {

	Route::apiResource('tasks', 'App\Http\Controllers\TaskController');

	Route::get('/user', 'App\Http\Controllers\UserController@currentUser');

	/*so sieht das aus, wenn eine neue, eigene Schnittstelle hinzugefügt wird -> Achtung! aufpassen das die urls der apiResourcen nicht doppelt belegt werden!*/
	Route::put('/task/{id}/update-status', 'App\Http\Controllers\TaskController@updateStatus');

});

